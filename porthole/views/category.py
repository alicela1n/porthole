#!/usr/bin/env python3

"""
    Porthole Views
    The view filter classes

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2009 Fredrik Arnerup, Daniel G. Taylor, Brian Dolbec,
    Brian Bockelman, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranSty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

""" Category Model

class CategoryView(Gtk.TreeView) 
Use for the cat view which is controlled by the filter view (combo box).  Sets up TreeView,
TreeViewColumn, TreeStore.

_clicked : handler for a click the the category window/tree
populate : populate the category view based on filter view selection. Called by view_filter_changed, 
           the combobox signal handler, in mainwindow.py
"""

import datetime
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject
from gettext import gettext as _

from porthole.views.helpers import *
from porthole.views.models import C_ITEM

logger = logging.getLogger(__name__)
logger.debug("CATEGORY: id initialized to %d", datetime.datetime.now().microsecond)


# self.Gtk.TreeView
# |
# |-- Gtk.TreeViewColumn Categories (short_name / full_name)
# |                     Gtk.CellRendererText()
# |
# |-- Gtk.TreeViewColumn _latest_column (count)
#                       Gtk.CellRendererText()
#
#   Gtk.TreeStore - model (str, str, int)
#
#   connect("cursor-changed", self._clicked)

class CategoryView(Gtk.TreeView):
    """ Self contained treeview to hold categories """

    def __init__(self):
        """ Initialize """
        # initialize the treeview
        Gtk.TreeView.__init__(self)
        # set last selected
        self._last_selected = None
        self._category_changed = None

        # ***** setup the Categories column *****
        self.cat_column = Gtk.TreeViewColumn(_("Categories"),
                                             Gtk.CellRendererText(editable=False),
                                             markup=C_ITEM["short_name"])
        self.append_column(self.cat_column)  # add column to TreeView
        self.cat_column.set_visible(True)    # column visable
        self.cat_column.set_expand(True)     # set column to take available extra space

        # ***** setup the Package column *****
        self.count_column = Gtk.TreeViewColumn(_("# pkgs"),
                                               Gtk.CellRendererText(editable=False),
                                               markup=C_ITEM["count"])
        self.append_column(self.count_column)  # add column to TreeView
        self.count_column.set_visible(True)    # column visable
        self.count_column.set_expand(False)    # set column to take available extra space

        # ***** setup the model  *****
        self.model = Gtk.TreeStore(GObject.TYPE_STRING, GObject.TYPE_STRING, GObject.TYPE_INT)
        self.set_model(self.model)

        # ***** connect to clicked event *****
        self.last_category = None
        self.connect("cursor-changed", self._clicked)  # position of the cursor has changed
        self.register_callback()                       # register default callback
        self.search_cat = False                        # initial set - search false, show cats
        self.show_all()                                # show yourself
        logger.debug("CATEGORY: VIEWS: Category view initialized")

    def set_search(self, option):
        self.search_cat = option
        if option is True:
            self.cat_column.set_title(_("Search History"))
        elif option is False:
            self.cat_column.set_title(_("Categories"))

    def register_callback(self, category_changed=None):
        """ Register callbacks for events """
        self._category_changed = category_changed

    def _clicked(self, treeview, *args):
        """ Handle treeview clicks """
        # "cursor-changed" handler
        model, iter = treeview.get_selection().get_selected()
        if iter:
            category = model.get_value(iter, C_ITEM["full_name"])
        else:
            category = self.last_category
        # has the selection really changed?
        if category is not self.last_category:
            logger.debug("VIEWS: category change detected")
            # then call the callback if it exists!
            if self._category_changed:
                self.last_category = category
                # category_changed in mainwindow.py
                self._category_changed(category)
        # save current selection as last selected
        self.last_category = category

    def populate(self, categories, _sort=True, counts=None):
        """Fill the category tree."""
        # This handles filter view (combobox) All and Installed

        # clear all rows to ready for tree reload
        self.model.clear()
        last_full_names = []

        # handle search filter view
        if self.search_cat is True:
            logger.debug("VIEWS: Populating category view; search_cat is True")
            self.populate_search(categories, counts)
            return

        # set parent_iter to top level
        parent_iter = [None]
        for cat in categories:
            logger.debug("VIEWS: CategoryView.populate(): cat: %s", cat)
            if cat:  # != 'virtual':
                cat_split = cat.split("-")
                max_level = len(cat_split) - 1

                # cats len is going to be 2 expcept for something like virtual or pentoo
                # as examples which have no children so len 1
                for i in range(len(cat_split)):
                    full_name = '-'.join(cat_split[:i + 1] or cat_split[0])
                    if i < max_level:
                        # add parent/subparent row
                        len_full_names = len(last_full_names)
                        if len_full_names > i and last_full_names[i] == full_name:
                            continue  # skip to the next level
                        # delete any previous deeper levels
                        if i > 0:
                            parent_iter = parent_iter[:i + 1]
                            last_full_names = last_full_names[:i]
                        else:
                            parent_iter = [None]
                            last_full_names = []
                        last_full_names.append(full_name)

                        parent_iter.append(self.model.insert_before(parent_iter[i], None))
                        self.model.set_value(parent_iter[i + 1], C_ITEM["short_name"], cat_split[i])
                        self.model.set_value(parent_iter[i + 1], C_ITEM["full_name"],
                                             None)  # last_full_names[i]) # needed?
                        self.model.set_value(parent_iter[i + 1], C_ITEM["count"], 0)
                    else:  # last one, short_name, i == max_level
                        # child row
                        parent_iter = parent_iter[:i + 1]
                        last_full_names.append(full_name)
                        parent_iter.append(self.model.insert_before(parent_iter[i], None))
                        self.model.set_value(parent_iter[i + 1], C_ITEM["short_name"], cat_split[i])
                        self.model.set_value(parent_iter[i + 1], C_ITEM["full_name"], full_name)
                        if counts is not None:  # and counts[cat] != 0:
                            self.model.set_value(parent_iter[i + 1], C_ITEM["count"], counts[cat])
                            path = self.model.get_path(parent_iter[i + 1])
                            p = i
                            while p > 0:
                                path = path[:p]
                                iter = self.model.get_iter(path)
                                prev_count = self.model.get_value(iter, C_ITEM["count"])
                                self.model.set_value(iter, C_ITEM["count"], counts[cat] + int(prev_count))
                                p -= 1
        logger.debug("VIEWS: CategoryView.populate() end routine")

    def populate_search(self, categories, counts):
        logger.debug("VIEWS: populating category view with search history")
        for string in categories:
            iter = self.model.insert_before(None, None)
            self.model.set_value(iter, C_ITEM["short_name"], string)
            self.model.set_value(iter, C_ITEM["full_name"], string)
            if counts is not None:  # and counts[string] != 0:
                # debug.dprint("VIEWS: Counts: %s = %s" %(cat, str(counts[string])))
                self.model.set_value(iter, C_ITEM["count"], counts[string])

    def clear(self):
        """ Clear current view """
        # get the treemodel
        model = self.get_model()
        if model is not None:
            # clear it
            # self.set_model(None)
            pass
