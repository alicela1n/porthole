#!/usr/bin/env python3

"""
    Porthole Utils Package
    Holds common variables for Porthole

    Fixed for Python 3 - March 2020 Michael Greene

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# ------------------------------------------------------------------------------
# Global class that holds paths and important variables. This is a new file
# for the Python 3 version which resulted in changes in other modules.
# ------------------------------------------------------------------------------

import os
import sys
import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("PATHS: id initialized to %d", datetime.datetime.now().microsecond)


class PortholePaths(object):

    def __init__(self):
        # ******* define paths ***********
        #
        # I left in old comments and code until I was sure unneeded - MKG
        #
        # Add path to portage module if
        # missing from path (ref bug # 924100)
        portage_mod_path = '/usr/lib/portage/pym'
        if portage_mod_path not in sys.path:
            sys.path.append(portage_mod_path)
        # ~ GENTOOLKIT_PATH = '/usr/lib/gentoolkit/pym'
        # ~ if GENTOOLKIT_PATH not in sys.path:
        # ~ sys.path.append(GENTOOLKIT_PATH)

        # while '' in sys.path: # we don't need the cwd in the path
        # assume this is still true running installed - MKG
        while '/usr/bin' in sys.path:  # this gets added when we run /usr/bin/porthole
            sys.path.remove('/usr/bin')

        self.APP = 'porthole'
        self.LOG_FILE_DIR = "/var/log/porthole"
        self.DB_FILE_DIR = "/var/db/porthole"
        self.i18n_DIR = '/usr/share/locale/'
        self.DIR_LIST = [self.LOG_FILE_DIR, self.DB_FILE_DIR]
        self.Choices = {"portage": 'portagelib', "pkgcore": 'pkgcore_lib', "dbus": "dbus_main"}

        # This is default when running installed
        self.DATA_PATH = "/usr/share/porthole/"
        self.RUN_LOCAL = False
        self.BACKEND = ""
        self.DEBUG = False
        self.NO_PRIV = False
        self.USEROPT = "None"
        self.MAKE_CONF = None
        self.session = None

        """
        check if directory exists, if not create it -- for new installs
        create db and log directory - was in startup
        """
        for _dir in self.DIR_LIST:
            if not os.access(_dir, os.F_OK):
                logger.debug("STARTUP: create_dir; %s does not exist, creating...",  _dir)
                try:
                    os.mkdir(_dir, int('0755'))
                except OSError:
                    logger.debug("Failed to create %s:", _dir)

    def get_app_name(self):
        return self.APP

    def get_log_path(self):
        return self.LOG_FILE_DIR

    def get_db_path(self):
        return self.DB_FILE_DIR

    def get_i18n_path(self):
        return self.i18n_DIR

    def set_i18n_path(self, i18n):
        self.i18n_DIR = i18n

    def set_data_path(self, data_path):
        self.DATA_PATH = data_path

    def get_data_path(self):
        return self.DATA_PATH

    def set_makeconf_path(self, makeconf_path):
        self.MAKE_CONF = makeconf_path

    def get_makeconf_path(self):
        return self.MAKE_CONF

    # From porthole start args - set internal BACKEND. Called at start, I do not see
    # if it changes or whether it even matters right now
    def set_backend(self, args_backend):
        self.BACKEND = self.Choices[args_backend]
        return

    def get_backend(self):
        return self.BACKEND

    # App debug setting from command args/default
    def set_debug(self, args_debug):
        self.DEBUG = args_debug

    def get_debug(self):
        return self.DEBUG

    # App local setting from command args/default
    def set_run_local(self, run_local):
        self.RUN_LOCAL = run_local

    def get_run_local(self):
        return self.RUN_LOCAL

    # App NOT try to run with root privileges
    # Is this hooked up ?? - MKG
    def set_no_priv(self, no_priv):
        self.NO_PRIV = no_priv

    def get_no_priv(self):
        return self.NO_PRIV

    # App user to run as for certain operations
    # Is this hooked up ?? - MKG
    def set_user(self, user):
        self.USEROPT = user

    def get_user(self):
        return self.USEROPT

    # For testing
    def path_dump(self):
        logger.debug("DATA_PATH:    " + self.DATA_PATH)
        if self.RUN_LOCAL:
            _RUN_LOCAL = "TRUE"
        else:
            _RUN_LOCAL = "FALSE"
        logger.debug("RUN_LOCAL:    " + _RUN_LOCAL)
        logger.debug("BACKEND:      " + self.BACKEND)
        if self.DEBUG:
            _DEBUG = "TRUE"
        else:
            _DEBUG = "FALSE"
        logger.debug("DEBUG:        " + _DEBUG)
        if self.NO_PRIV:
            _NO_PRIV = "TRUE"
        else:
            _NO_PRIV = "FALSE"
        logger.debug("NO_PRIV:      " + _NO_PRIV)
        logger.debug("USEROPT:      " + self.USEROPT)
        logger.debug("APP:          " + self.APP)
        logger.debug("LOG_FILE_DIR: " + self.LOG_FILE_DIR)
        logger.debug("DB_FILE_DIR:  " + self.DB_FILE_DIR)
        logger.debug("i18n_DIR:     " + self.i18n_DIR)
