#!/usr/bin/env python3

"""
    Porthole About Dialog
    Shows information about Porthole

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup and Daniel G. Taylor

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from porthole.config import Config
import webbrowser


class AboutDialog:
    """Class to hold about dialog and functionality."""

    def __init__(self):
        from porthole.config import Paths
        self._DATA_PATH = Paths.get_data_path()  # config.Prefs.use_gladefile
        self.author_file = Config.Prefs.AUTHORS
        # setup glade
        self.gladefile = self._DATA_PATH + 'glade/about.glade'
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)
        # register callbacks
        callbacks = {"on_ok_clicked": self.ok_clicked,
                     "on_homepage_clicked": self.homepage_clicked}
        self.builder.connect_signals(callbacks)
        self.copyright = Gtk.Builder.get_object(self.builder, 'copyright_label')
        # Main window
        self.window = Gtk.Builder.get_object(self.builder, "about_dialog")

    def ok_clicked(self, widget):
        """Get rid of the about dialog!"""
        self.builder.get_object("about_dialog").destroy()

    def homepage_clicked(self, widget):
        """Open Porthole's Homepage!"""
        webbrowser.open('https://gitlab.com/mikeos2/porthole/-/wikis/Porthole3')
