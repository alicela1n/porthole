#!/usr/bin/env python3

"""
    ============
    | File Save |
    -----------------------------------------------------------
    Copyright (C) 2003 - 2008 Fredrik Arnerup, Brian Dolbec, 
    Daniel G. Taylor, Wm. F. Wheeler, Tommy Iorns

    Fixed for Python 3 - April 2020 Michael Greene

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    -------------------------------------------------------------------------
    To use this program as a module:
    
        from fileselector import FileSelector
"""

import datetime
import logging

logger = logging.getLogger(__name__)

logger.debug("FILESELECTOR: id initialized to %d", datetime.datetime.now().microsecond)

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import os, os.path
from porthole.utils import debug

class FileSelector2:
    """Generic file selector dialog for opening or saving files"""
    
    def __init__(self, parent_window, target_path, callback = None, overwrite_confirm = True, filter = None):
        debug.dprint("FILESELECTOR2: __init__(); # 109 target_path = %s" %str(target_path))
        self.window = parent_window
        self.callback = callback
        self.overwrite_confirm = overwrite_confirm
        if os.path.isdir(target_path):
            self.directory = target_path
            debug.dprint("FILESELECTOR2: __init__(); # 115 directory = %s" %str(self.directory))
            self.target = ''
        elif os.path.isfile(target_path):
            self.target = target_path
            self.directory, file = os.path.split(target_path)
            debug.dprint("FILESELECTOR2: __init__(); # 120 directory = %s" %str(self.directory))
        else:
            self.directory, self.target  =os.path.split(target_path)
            debug.dprint("FILESELECTOR2: __init__(); # 123 directory = %s" %str(self.directory))
        self.filter = filter
        
        self.actions = {'save': Gtk.FILE_CHOOSER_ACTION_SAVE,
                                'open': gtk.FILE_CHOOSER_ACTION_OPEN,
                                'select_folder': gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                'create_folder': gtk.FILE_CHOOSER_ACTION_CREATE_FOLDER
                                }


    def create_selector(self, title, action):
        debug.dprint("FILESELECTOR2: Entering create_selector()")
        buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                    gtk.STOCK_OK, gtk.RESPONSE_OK)
        self.dialog = gtk.FileChooserDialog(title, self.window, self.actions[action], buttons)
        self.dialog.set_do_overwrite_confirmation(self.overwrite_confirm)
        if self.directory:
            debug.dprint("FILESELECTOR2: create_selector(); # 142 directory = %s" %str(self.directory))
            self.dialog.set_current_folder(self.directory)
        if self.target and'/' in self.target:
            self.dialog.set_filename(self.target)
        else:
            self.dialog.set_current_name(self.target)
        show_all = gtk.FileFilter()
        show_all.add_pattern('*')
        show_all.set_name('All')
        self.dialog.add_filter(show_all)
        if self.filter: # add a specified filter string
            filter = gtk.FileFilter()
            filter.add_pattern(self.filter)
            filter.set_name(self.filter.split('.')[1] +' Files')
            debug.dprint("FILESELECTOR2: create_selector(); filter name = " + filter.get_name() )
            self.dialog.add_filter(filter)
            self.dialog.set_filter(filter)
        else:
            self.dialog.set_filter(show_all)
        
    def get_filename(self, title, action):
        debug.dprint("FILESELECTOR2: Entering get_filename()")
        self.create_selector(title, action)
        result = self.dialog.run()
        debug.dprint("FILESELECTOR2: get_filename(); result = " + str(result))
        if result in [gtk.RESPONSE_OK, gtk.RESPONSE_ACCEPT, gtk.RESPONSE_YES]: 
            filename = self.dialog.get_filename()
        elif result in [gtk.RESPONSE_CANCEL, gtk.RESPONSE_DELETE_EVENT, gtk.RESPONSE_CLOSE]:
            filename = ''
        self.dialog.destroy()
        debug.dprint("FILESELECTOR2: get_filename(); filename = " + filename)
        return filename
